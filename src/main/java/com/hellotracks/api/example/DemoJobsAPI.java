package com.hellotracks.api.example;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@SuppressWarnings("unchecked")
public class DemoJobsAPI {

	static final String HOST = "https://hellotracks.com/api/";

	static final String CREATEJOBS = HOST + "createjobs";
	static final String GETJOBS = HOST + "getjobs";
	static final String EDITJOBS = HOST + "editjobs";
	static final String DELETEJOBS = HOST + "deletejobs";
	static final String DISTRIBUTEJOBS = HOST + "distributejobs";
	static final String OPTIMIZEROUTE = HOST + "optimizeroute";
	static final String LOCATE = HOST + "locate";
	static final String GETTRACKS = HOST + "gettracks";
	static final String EDITACCOUNT = HOST + "editaccount";

	// Configuration for Authentication
	// XXX These fields need to be adjusted to your account
	static final String USERNAME = ""; // username "topservicesltd@gmail.com"
	static final String PASSWORD = ""; // password e.g. "password-123";
	static final String API_KEY = ""; // API key e.g. "691A5E9A1A523590C47ED4908393EE07"
	
	static JSONObject prepareObj(JSONObject root) {
		final long now = System.currentTimeMillis();
		final int cts = (int) (now / 1000L);

		final String tok = String.valueOf(System.currentTimeMillis()); // always new token as string
		final String usr = USERNAME;
		final String md5 = Utils.md5(tok, PASSWORD, "" + cts);
		final String key = API_KEY;

		JSONObject auth = new JSONObject();
		auth.put("usr", usr);
		auth.put("cts", cts);
		auth.put("tok", tok);
		auth.put("pwd", md5);
		auth.put("key", key);

		root.put("auth", auth);

		JSONObject data = new JSONObject();
		root.put("data", data);
		return data;
	}

	/*
	 * @See https://hellotracks.com/jobs-api.html#createjobs
	 */
	private void createJob() {
		JSONObject job = new JSONObject();
		job.put("destinationName", "Hello from API");
		job.put("destinationLat", 37.7953073);
		job.put("destinationLng", -122.3951383);
		job.put("orderId", 1234567);
		job.put("teamId", 4); // NEW: assigned to team #4 (0 is no specific team, 1-6 referring to the team
								// number)
		job.put("tsCreated", System.currentTimeMillis());
		job.put("number", 1);
		job.put("day", Utils.dayOf(System.currentTimeMillis())); // e.g. 20151116
		job.put("textDispatcher", "You need to do the following..... ");
		job.put("contactName", "The contact name");
		job.put("contactPhone", "+1 1234567");

		// NEW: custom fields
		job.put("custom_Hello", "World!");
		job.put("custom_Another Field", "1234");

		// for more fields see: http://hellotracks.com/jobs-api.html#api-object

		JSONArray jobs = new JSONArray();
		jobs.add(job);

		JSONObject params = new JSONObject();
		JSONObject data = prepareObj(params);
		data.put("jobs", jobs);

		Utils.postAndPrint(CREATEJOBS, params);
	}

	/*
	 * @See https://hellotracks.com/jobs-api.html#editjobs
	 */
	private void editJob() {
		JSONObject job = new JSONObject();
		job.put("destinationName", "Hello from API ABABAB");
		job.put("contactPhone", "+1 1234567 bbb");

		JSONObject obj = new JSONObject();
		obj.put("56c7766f4afabf90b8f5f077", job);
		JSONObject params = new JSONObject();
		JSONObject data = prepareObj(params);
		data.put("jobs", obj);
		Utils.postAndPrint(EDITJOBS, params);
	}

	/*
	 * @See https://hellotracks.com/jobs-api.html#getjobs
	 */
	private JSONObject getAllJobsForDay(int day) {
		JSONObject params = new JSONObject();
		JSONObject data = prepareObj(params);
		data.put("day", day);
		data.put("worker", "*");
		data.put("team", 0); // NEW: optionally you can filter by team 1-6
		return Utils.postAndPrint(GETJOBS, params);
	}

	/*
	 * @See https://hellotracks.com/jobs-api.html#locate
	 */
	private void locateAccount() {
		String username = "daniel.bernoulli.ts";
		JSONObject params = new JSONObject();
		JSONObject data = prepareObj(params);
		JSONObject accounts = new JSONObject();
		accounts.put(username, new JSONObject());
		data.put("accounts", accounts);
		data.put("withAddress", true);
		data.put("withDetails", true);
		data.put("signal", "ping");

		Utils.postAndPrint(LOCATE, params);
	}

	/*
	 * @See https://hellotracks.com/jobs-api.html#locate
	 */
	private void locateTeam(int teamId) {
		JSONObject params = new JSONObject();
		JSONObject data = prepareObj(params);

		JSONArray teams = new JSONArray();
		teams.add(teamId);
		data.put("teams", teams); // NEW: locate entire teams
		data.put("withAddress", false);

		Utils.postAndPrint(LOCATE, params);
	}

	/*
	 * @See https://hellotracks.com/jobs-api.html#editaccount
	 */
	private void editAccount() {
		JSONObject params = new JSONObject();
		JSONObject data = prepareObj(params);

		data.put("account", "topservicesltd@gmail.com");
		data.put("status_label", "Test Status " + new Date());

		Utils.postAndPrint(EDITACCOUNT, params);
	}

	/*
	 * @See https://hellotracks.com/jobs-api.html#gettracks
	 */
	private void getTracks() {
		JSONObject params = new JSONObject();
		JSONObject data = prepareObj(params);

		JSONObject accounts = new JSONObject();
		accounts.put("daniel.bernoulli.ts", new JSONObject());
		data.put("accounts", accounts);

		data.put("withCourse", true);
		data.put("from", 1443664616000L);
		data.put("until", System.currentTimeMillis());

		Utils.postAndPrint(GETTRACKS, params);
	}

	/*
	 * @See https://hellotracks.com/jobs-api.html#distributejobs
	 */
	private JSONObject distributeJobs(List<String> jobIds) {
		JSONObject params = new JSONObject();
		JSONObject data = prepareObj(params);

		JSONObject allJobs = new JSONObject();
		for (String id : jobIds) {
			allJobs.put(id, new JSONObject());
		}
		data.put("jobs", allJobs);
		data.put("regions", 3);
		data.put("optimize", 1); // 1: even cluster size, 2: distance

		return Utils.postAndPrint(DISTRIBUTEJOBS, params);
	}

	/*
	 * @See https://hellotracks.com/jobs-api.html#editjobs
	 */
	private JSONObject assignJobs(String worker, int day, List<String> ids) {
		JSONObject params = new JSONObject();
		JSONObject data = prepareObj(params);

		JSONObject jobs = new JSONObject();
		for (String id : ids) {
			JSONObject obj = new JSONObject();
			obj.put("day", day);
			obj.put("worker", worker);
			jobs.put(id, obj);
		}
		data.put("jobs", jobs);
		return Utils.postAndPrint(EDITJOBS, params);
	}

	/*
	 * @See https://hellotracks.com/jobs-api.html#editjobs
	 */
	private JSONObject setSuccessTS(String jobId1, String jobId2, String jobId3) {
		JSONObject params = new JSONObject();
		JSONObject data = prepareObj(params);
		JSONObject jobs = new JSONObject();
		{
			JSONObject obj = new JSONObject();
			obj.put("tsDoneSuccess", System.currentTimeMillis());
			jobs.put(jobId1, obj);
		}
		{
			JSONObject obj = new JSONObject();
			obj.put("tsDoneFailed", System.currentTimeMillis());
			jobs.put(jobId2, obj);
		}
		{
			JSONObject obj = new JSONObject();
			obj.put("tsAccepted", 0);
			obj.put("tsDoneSuccess", 0);
			obj.put("tsDoneFailed", 0);
			jobs.put(jobId3, obj);
		}
		data.put("jobs", jobs);
		return Utils.postAndPrint(EDITJOBS, params);
	}

	/*
	 * @See https://hellotracks.com/jobs-api.html#optimizeroute
	 */
	private JSONObject optimizeRoute(String worker, int day) {
		JSONObject params = new JSONObject();
		JSONObject data = prepareObj(params);

		data.put("day", day);
		data.put("includeCompany", true);
		data.put("account", worker);
		return Utils.postAndPrint(OPTIMIZEROUTE, params);
	}

	/*
	 * @See https://hellotracks.com/jobs-api.html
	 */
	private void advancedJobFlow() {
		int DAY = 20151024;
		int TODAY = Utils.dayOf(System.currentTimeMillis());

		// let's assume we have already jobs on this day
		JSONObject res = getAllJobsForDay(DAY);
		JSONArray arr = (JSONArray) res.get("jobs");
		LinkedList<String> jobIds = new LinkedList<>();
		for (int i = 0; i < arr.size(); i++) {
			JSONObject obj = (JSONObject) arr.get(i);
			jobIds.add((String) obj.get("id"));
		}
		JSONObject distributed = distributeJobs(jobIds);

		JSONArray clusters = (JSONArray) distributed.get("clusters");

		if (clusters != null && clusters.size() > 0) {
			// in this example we only assign the first cluster to one member
			JSONObject cluster = (JSONObject) clusters.get(0);
			JSONArray jobs = (JSONArray) cluster.get("jobs");
			List<String> ids = new LinkedList<>();
			for (int j = 0; j < jobs.size(); j++) {
				ids.add((String) jobs.get(j));
			}
		}

		assignJobs("daniel.bernoulli.ts", TODAY, jobIds);
		optimizeRoute("daniel.bernoulli.ts", TODAY);
	}

	public static void main(String[] args) {
		DemoJobsAPI api = new DemoJobsAPI();

		// example for locating accounts
		api.locateAccount();

		// example for locating all accounts of a team
		api.locateTeam(0);

		// example of editing a field of an account
		api.editAccount();

		// example for retrieving mileage and more track info
		api.getTracks();

		// example for creating a job
		api.createJob();
		api.editJob();

		// advance job actions like distributing and optimizing job routes
		api.advancedJobFlow();

		api.getAllJobsForDay(20171217);
		api.setSuccessTS("57a2bdd6756260247b06d412", "57c9317f7562604a94788af7", "57c9317f7562604a94788b00");
	}

}
