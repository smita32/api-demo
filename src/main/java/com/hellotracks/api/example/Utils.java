package com.hellotracks.api.example;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Calendar;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Utils {

	public static void close(Closeable reader) {
		if (reader != null) {
			try {
				reader.close();
			} catch (IOException exc) {
				exc.printStackTrace();
			}
		}
	}

	public static JSONObject postAndPrint(String urlString, JSONObject postData) {
		BufferedReader reader = null;
		BufferedWriter writer = null;
		OutputStream os = null;
		try {
			URL url = new URL(urlString);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(20000);
			conn.setConnectTimeout(6000);
			conn.setRequestMethod("POST");
			conn.setDoInput(true);
			conn.setDoOutput(true);

			os = conn.getOutputStream();
			writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
			writer.write(postData.toJSONString());
			writer.flush();
			close(writer);
			close(os);

			conn.connect();
			
			reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			final StringBuilder builder = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
			
			JSONObject response = (JSONObject) new JSONParser().parse(builder.toString());
			print(urlString, postData, response);
			return response;
		} catch (Exception exc) {
			close(writer);
			close(os);
			exc.printStackTrace();
			return null;
		} finally {
			close(reader);
		}
	}
	
	public static String md5(String nonce, String password, String cts) {
		try {
			// Create MD5 Hash
			String plain = nonce + password + cts;
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(plain.getBytes());
			byte[] messageDigest = md.digest();

			// Create Hex String
			StringBuilder hexString = new StringBuilder();
			for (int i = 0; i < messageDigest.length; i++) {
				hexString.append(String.format("%02X", messageDigest[i]));
			}
			String pwd = hexString.toString();
			return pwd;
		} catch (Exception exc) {
			exc.printStackTrace();
		}
		return "";
	}
	
	public static int dayOf(long ts) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(ts);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);

		StringBuilder sb = new StringBuilder();
		sb.append(year);
		if (month < 10) {
			sb.append(0);
		}
		sb.append(month);
		if (dayOfMonth < 10) {
			sb.append(0);
		}
		sb.append(dayOfMonth);
		int day = Integer.parseInt(sb.toString());
		return day;
	}
	
	private static void print(String title, JSONObject req, JSONObject res) {
		System.out.println("******* " + title + "  *******");
		System.out.println("req: " + req.toJSONString());
		System.out.println("res: " + res.toJSONString());
		System.out.println("**********************\n");
	}
}
